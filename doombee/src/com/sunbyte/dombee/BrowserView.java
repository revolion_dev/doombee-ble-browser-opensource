package com.sunbyte.dombee;

import java.util.ArrayList;
import java.util.Random;

import org.openawt.draw.android.ShapePainter;

import com.sunbyte.classes.BLEDevice;
import com.sunbyte.classes.Circle;
import com.sunbyte.classes.CirclePacking;

import kn.uni.voronoitreemap.datastructure.OpenList;
import kn.uni.voronoitreemap.diagram.PowerDiagram;
import kn.uni.voronoitreemap.j2d.Point2D;
import kn.uni.voronoitreemap.j2d.PolygonSimple;
import kn.uni.voronoitreemap.j2d.Site;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.view.View;

public class BrowserView extends SurfaceView implements View.OnTouchListener {

	public PowerDiagram diagram;
	OpenList sites = new OpenList();
	
	Canvas thiscanvas;
	Boolean fromHover = false;
	PolygonSimple hoverPoly;
	
	int width; // screen width
	int height; // screen height
	
	int minSites = 0;
	int numOfSites = 0;
	
	public Boolean DistancesGridActive;
	
	CirclePacking cp;
	
	
	public void RefreshBrowserDesignForNewDevice(int numOfDevices, 
									ArrayList<BLEDevice> listOfDevices) {
		
		
		
		if (numOfDevices < minSites) {
			numOfSites = minSites;
		} else {
			minSites = 10;
			
			numOfSites = numOfDevices;
			if (numOfSites < minSites) {
				numOfSites = minSites;
			}

			// TODO save sites status
			sites.clear();
			// VORONOI
			diagram = new PowerDiagram();
			
	
			Random rand = new Random();
			// create a root polygon which limits the voronoi diagram.
			//  here it is just a rectangle.
	
			PolygonSimple rootPolygon = new PolygonSimple();
			
			// CIRCLE PACKING FOR WEIGHTED TASSELATION. LOADING CIRCLES
			cp = new CirclePacking(width, height);
			
			for (int i=0; i<numOfSites; i++)
			{
				int nextWidth = rand.nextInt(width);
				int nextHeight = rand.nextInt(height);
				int weight = rand.nextInt(10);
				if (weight == 0) weight = 1;
				System.out.println("WI " + nextWidth + " " + "HE " + nextHeight);
				cp.nodes.add(new Circle(nextWidth, nextHeight, "", weight));
			}
			
			cp.ChangeRadiusesByWeight();
			cp.packAllCircles();
			
			rootPolygon.add(0, 0);
			rootPolygon.add(width, 0);
			rootPolygon.add(width, height);
			rootPolygon.add(0, height);     
	
			ArrayList<Point2D> sitePoints = cp.getCirclePoints();
			// create 100 points (sites) and set random positions in the rectangle defined above.
		    for (int i = 0; i < numOfSites; i++) {
		    	
		    	Point2D sitepoint = sitePoints.get(i);
			    
		        Site site = new Site(sitepoint.x, sitepoint.y);
		        // we could also set a different weighting to some sites
		        site.setWeight(sitepoint.weight);

		        sites.add(site);
		    }
	
			// set the list of points (sites), necessary for the power diagram
			diagram.setSites(sites);
			// set the clipping polygon, which limits the power voronoi diagram
			diagram.setClipPoly(rootPolygon);
	
			// do the computation
			diagram.computeDiagram();   

		}
		// for each site we can no get the resulting polygon of its cell. 
		// note that the cell can also be empty, in this case there is no polygon for the corresponding site.
        for (int i=0;i<sites.size;i++){
            Site site=sites.array[i];
            PolygonSimple polygon=site.getPolygon();
            
            if (polygon != null) {
            	
            	// generate colors for polygon
            	if (listOfDevices.size() > i) {
            		
            		BLEDevice device = listOfDevices.get(i);
            		site.setDevice(device);
            		
            		
            		if (device.getColor() != null) {
            			polygon.setFillColor(device.getColor());
            		} else {
            			int newcolor = polygon.generateRandomColor(numOfDevices);
            			device.setColor(newcolor);
            			polygon.setFillColor(newcolor);
            		}
            		
	        	} else {
	        		// site without devices. set a disabled gray color
	        		polygon.setFillColor(Color.rgb(216, 216, 216)); // LTGray	
	        	}
            }
            
            site.setPolygon(polygon);
            
        }
        
       
		
		
	}
	
	
	public BrowserView(Context context, int numOfDevices, ArrayList<BLEDevice> listOfDevices) {
		super(context);
		
		setWillNotDraw(false);
		
		this.setOnTouchListener(this);
		
		// get display
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();

		width = display.getWidth();
		height = display.getHeight();
		
		this.DistancesGridActive = true;
		
		this.RefreshBrowserDesignForNewDevice(numOfDevices, listOfDevices);

				
		        
	}
	
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		//System.out.println("ON DRAW");
		thiscanvas = canvas;
		
		
		// TEST
		Paint p = new Paint();
		p.setColor(Color.GRAY);
		
		p.setStyle(Style.STROKE);
		p.setAntiAlias(true);
		

		// END TEST
		
		
		if (!this.fromHover) {
			
			diagram.setCanvas(canvas);
			diagram.showDiagram(DistancesGridActive);
			
			
		} else {
			diagram.setCanvas(canvas);
			diagram.showDiagram(DistancesGridActive);
			drawHoverSite(this.hoverPoly);
		
		}
		
		
		//thiscanvas.save();


	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_UP) {
			
			// loop through all polygons and check for clicked site
	        for (int i=0;i<sites.size;i++){
	            Site site=sites.array[i];
	            PolygonSimple polygon=site.getPolygon();
	            
	            Point2D p2d = new Point2D();
	            p2d.x = event.getRawX();
	            p2d.y = event.getRawY();
	            
	           // System.out.println(p2d.x);
	           // System.out.println(p2d.y);
	            
	            if (polygon.contains(p2d)) {
	            	//System.out.println("Polygon contains");
	            	
	            	if (event.getAction() == MotionEvent.ACTION_DOWN) { 
	            		this.fromHover = true;
	            		this.hoverPoly = polygon;
	            		invalidate();
	            		return true;
	            	} 
	            	
	            	if (event.getAction() == MotionEvent.ACTION_UP) { 
	            		this.fromHover = false;
	            		//this.hoverPoly = polygon;
	            		invalidate();
	            		
	            	} 
	            	// draw hover effect
	            	break;
	            }
	            
	        }
		}
		return false;
	}
	
	
	
	public int darker (int color, float factor) {
	    int a = Color.alpha( color );
	    int r = Color.red( color );
	    int g = Color.green( color );
	    int b = Color.blue( color );

	    return Color.argb( a,
	            Math.max( (int)(r * factor), 0 ),
	            Math.max( (int)(g * factor), 0 ),
	            Math.max( (int)(b * factor), 0 ) );
	}
	
	
	public void drawHoverSite(PolygonSimple poly) {
		
		if (poly != null) {
			
			System.out.println("Drawing Hover");
			Paint p = new Paint();
		
			int darkerColor = this.darker(poly.fillColor, 0.75f);
			p.setColor(darkerColor);
			
			p.setStyle(Style.FILL);
			p.setAntiAlias(true);
			ShapePainter.draw(thiscanvas, poly, p);
			
			p = new Paint();
			p.setColor(Color.GRAY);
			p.setStrokeWidth(5.5f);
			p.setStyle(Style.STROKE);
			p.setAntiAlias(true);
			ShapePainter.draw(thiscanvas, poly, p);
			//graphics.draw
			//graphics.draw(poly);
		} else {
			System.out.println("Poly null of");
		}
		
	}

}
