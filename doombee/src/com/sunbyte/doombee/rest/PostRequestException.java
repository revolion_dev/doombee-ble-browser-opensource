package com.sunbyte.doombee.rest;

public class PostRequestException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// constructor
	public PostRequestException(String message) {
		 
		 super(message); 
    } 


}
