package com.sunbyte.doombee.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.sunbyte.doombee.globals.ApplicationStatus;



import android.util.Log;

public class RestUtils {

	private final static String METHOD_POST = "post";
	private final static String METHOD_GET = "get";
	private final static String TAG = "RestUtils";
	private final static String TestUrl = ApplicationStatus.ServiceBase + "login/";
	private final static String TimeChangeHost = "http://wg.vod.ftctele.com";
	private final static String TimeChangePath = "/rvil/rest/event.php";
	
	
	
	public static boolean changeTime(long event_id, int start_time, int end_time) {
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("action", "changetime");
		parameters.put("event_pk", String.valueOf(event_id));
		parameters.put("time1", String.valueOf(start_time));
		parameters.put("time2", String.valueOf(end_time));
		try {
			if(doRESTRequest(TimeChangeHost, "get", TimeChangePath, parameters) != null ) {
				return true;
			} else {
				return false;
			}
		} catch (RestRequestException e) {
			return false;
		}
		
	}
	
	public static String parseISToString(java.io.InputStream is)
	{   
		String endStr = "";
		//java.io.DataInputStream din = new java.io.DataInputStream(is);
        String UTF8 = "windows-1252";
        int BUFFER_SIZE = 8192;
        
		BufferedReader br;
		try {
			br = new BufferedReader(new InputStreamReader(is,
					UTF8));
	        String str;
	        while ((str = br.readLine()) != null) {
	        	endStr += str + "\n";
	        }
	        
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
 
		return endStr;
	}
	 
	
	public static String doRESTRequest (String host, String method, String path, Map<String,String> parameters) 
			  throws RestRequestException{
			  
			  return doRESTRequest(host, "80", method, path, parameters);
			 }
			
	public static boolean isConnected() {
		try {
			Log.e("Connection"," Testing URL " +  TestUrl);
			DefaultHttpClient client = new DefaultHttpClient();
			URI uri = new URI(TestUrl);
			HttpGet method = new HttpGet(uri);
			HttpResponse res = client.execute(method);
			Log.e("Connection", "Status: " + res.getStatusLine().getStatusCode());
			if(res.getStatusLine().getStatusCode() == 200) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

			 /***
			  * Esegue una richiesta REST
			  * 
			  * @param host   : hostname
			  * @param port   : porta
			  * @param method  : methodo (GET,POST supportati per ora)
			  * @param path   : percorso
			  * @param parameters : parametri
			  * @return    : Stringa contenente la response
			  * @throws RestRequestException : Eccezione personalizzata
			  */
			 public static String doRESTRequest (String host, String port, String method, String path, Map<String,String> parameters) 
			  throws RestRequestException {
			  
			  String returnString = null;
			  try{
			    
			   String url = host+":"+port+path;
			   Log.i(TAG,METHOD_GET+" Request - URL : "+url );
			   
			   if (METHOD_GET.equalsIgnoreCase(method)){
			    returnString = doGETRequest(url,parameters); 
			   }else if (METHOD_POST.equalsIgnoreCase(method)){
			    returnString = doPOSTRequest(url, parameters);
			   }
			  }catch(GetRequestException e){
			   throw new RestRequestException(e.getMessage(), "");  
			  }catch(PostRequestException e){
			   throw new RestRequestException(e.getMessage(), "");
			  }
			  
			  return returnString;
			 }
			 
			 /***
			  * Metodo per eseguire una request di tipo GET
			  * @param url : url della request
			  * @return  : stringa contenente la response
			  * @throws GetRequestException : Eccezione modellata
			  */
			 public static String doGETRequest(String url, Map<String, String> aprameters) throws GetRequestException{
			  String paramStr = composeParametersForGetRequest(aprameters);
			  
			  if (paramStr!=null && paramStr.length() > 0 ) url = url +"?"+paramStr;
			  String websiteData = null;
			  
			  try {
			   Log.i(TAG,"GetRequest - url : "+url);
			   DefaultHttpClient client = new DefaultHttpClient();
			   URI uri = new URI(url);
			   HttpGet method = new HttpGet(uri);
			   HttpResponse res = client.execute(method);
			   InputStream data = res.getEntity().getContent();
			   websiteData = parseISToString(data);
			  // HttpEntity entity = res.getEntity();
			  // websiteData = EntityUtils.toString(entity, "UTF-8");
			   
			  } catch (ClientProtocolException e) {
			   throw new GetRequestException(e.getMessage());
			  } catch (IOException e) {
			   throw new GetRequestException(e.getMessage());
			  } catch (URISyntaxException e) {
			   throw new GetRequestException(e.getMessage());
			  } finally {
			   Log.i(TAG,"GetRequest - Request & Response completed");
			  }
			  return websiteData;
			 }
			 
			 
			 public static String doGETRequest(String url) throws GetRequestException{
				  String websiteData = null;
				  
				  try {
				   Log.i(TAG,"GetRequest - url : "+url);
				   DefaultHttpClient client = new DefaultHttpClient();
				   URI uri = new URI(url);
				   HttpGet method = new HttpGet(uri);
				   HttpResponse res = client.execute(method);
				   int code = res.getStatusLine().getStatusCode();
				   if (code != HttpStatus.SC_OK) {
					   return "Error";
				   }
				   InputStream data = res.getEntity().getContent();
				   websiteData = parseISToString(data);
				  } catch (ClientProtocolException e) {
				   throw new GetRequestException(e.getMessage());
				  } catch (IOException e) {
				   throw new GetRequestException(e.getMessage());
				  } catch (URISyntaxException e) {
				   throw new GetRequestException(e.getMessage());
				  } finally {
				   Log.i(TAG,"GetRequest - Request & Response completed");
				  }
				  return websiteData;
				 }
			 
			 
			 
			 public static String doDELETERequest(String url, Map<String, String> parameters) throws GetRequestException {
				 
				 parameters.put("method", "delete");
				 
				 String paramStr = composeParametersForGetRequest(parameters);
				 String returnString = null;
				 if (paramStr!=null && paramStr.length() > 0 ) url = url +"?"+paramStr;
				 
				  try {
					   Log.i(TAG,"GetRequest - url : "+url);
					   DefaultHttpClient client = new DefaultHttpClient();
					   URI uri = new URI(url);
					   HttpGet method = new HttpGet(uri);
					   HttpResponse res = client.execute(method);
					   InputStream data = res.getEntity().getContent();
					   
					   returnString = parseISToString(data);
					  } catch (ClientProtocolException e) {
					   throw new GetRequestException(e.getMessage());
					  } catch (IOException e) {
					   throw new GetRequestException(e.getMessage());
					  } catch (URISyntaxException e) {
					   throw new GetRequestException(e.getMessage());
					  } finally {
					   Log.i(TAG,"GetRequest - Request & Response completed");
					  }
				 
				 return returnString;
				 
			 }
			 
/*			 public static String doDELETERequest(String url, Map<String, String> parameters) {
				 
				 String paramStr = composeParametersForGetRequest(parameters);
				 String returnString = null;
				 if (paramStr!=null && paramStr.length() > 0 ) url = url +"?"+paramStr;
				
				 try {
					 HttpClient httpclient = new DefaultHttpClient();
					 HttpDelete httpdelete = new HttpDelete(url);
					 httpdelete.setHeader(HTTP.CONTENT_TYPE, "text/xml");
					 // Eseguo la richiesta HTTP
					 HttpResponse response;

					 response = httpclient.execute(httpdelete);
					 int code = response.getStatusLine().getStatusCode();
					 if (code != HttpStatus.SC_OK) {
						   return "Error " + code;
					 }
					 
					 InputStream data = response.getEntity().getContent();
					 returnString = parseISToString(data);

				 } catch (UnsupportedEncodingException e) {
					 e.printStackTrace();
				 } catch (ClientProtocolException e) {
					 e.printStackTrace();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
					 
				 return returnString;
			 }*/
			 
			 
			 public static String doPUTRequest(String url, Map<String, String> parameters) throws PostRequestException {

				 url = url + "/";
				 parameters.put("method", "update");
				 System.out.println("PUT URL:" + url);
				 System.out.println(parameters);

				  String returnString = null;
				     // Creo un nuovo HttpClient e l'Header del post
				     HttpClient httpclient = new DefaultHttpClient();
				     HttpPost httppost = new HttpPost(url);
				     //httppost.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded");
				     //httppost.setHeader(HTTP.CONTENT_TYPE, "text/xml");
				     //httppost.setHeader(HTTP.CONTENT_TYPE, "multipart/form-data");
				     httppost.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded");
				     
				     try {
				         // aggiungo i dati alla richiesta
				         List<NameValuePair> nameValuePairs = composeParametersForPostRequest(parameters);
				         httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

				         // Eseguo la richiesta HTTP
				         HttpResponse response = httpclient.execute(httppost);
				         
				         System.out.println("Status code PUT: " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
				         
				         if (response.getStatusLine().getStatusCode() == 200) { 
				          returnString = EntityUtils.toString(response.getEntity()); 
				          		System.out.println("Status PUT" + returnString);
				                response = null; 
				         } else {
				        	 returnString = "Error";
				         }
				         
				     } catch (ClientProtocolException e) {
				      throw new PostRequestException(e.getMessage());
				     } catch (IOException e) {
				      throw new PostRequestException(e.getMessage());
				     }
				     
				     return returnString;
				
			 }
			 
/*			 public static String doPUTRequest(String url, Map<String, String> parameters) {
				 String returnString = null;
				 
				 HttpClient httpclient = new DefaultHttpClient();
				 HttpPut httpput = new HttpPut(url);
				 httpput.setHeader(HTTP.CONTENT_TYPE, "text/xml");
				 
				 try {
					 List<NameValuePair> nameValuePairs = composeParametersForPostRequest(parameters);

					 httpput.setEntity(new UrlEncodedFormEntity(nameValuePairs));

					 // Eseguo la richiesta HTTP
					 HttpResponse response;

					 response = httpclient.execute(httpput);
					 
					 System.out.println("Status code: " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());

					 if (response.getStatusLine().getStatusCode() == 200) { 
						 returnString = EntityUtils.toString(response.getEntity()); 
						 response = null; 
					 } else {
						 returnString = "Error";
					 }
					 
				 } catch (UnsupportedEncodingException e) {
					 e.printStackTrace();
				 } catch (ClientProtocolException e) {
					 e.printStackTrace();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
					 

				 return returnString;
			 }*/
			 
			 public static String doPOSTRequest(String url, Map<String, String> parameters) 
			  throws PostRequestException{
			  
				 url = url + "/";
			  String returnString = null;
			     // Creo un nuovo HttpClient e l'Header del post
			     HttpClient httpclient = new DefaultHttpClient();
			     HttpPost httppost = new HttpPost(url);
			     //httppost.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded");
			     //httppost.setHeader(HTTP.CONTENT_TYPE, "text/xml");
			     httppost.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded");
			     
			     try {
			         // aggiungo i dati alla richiesta
			         List<NameValuePair> nameValuePairs = composeParametersForPostRequest(parameters);
			         httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

			         // Eseguo la richiesta HTTP
			         HttpResponse response = httpclient.execute(httppost);
			         
			         System.out.println("Status code: " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
			         
			         if (response.getStatusLine().getStatusCode() == 200) { 
			          returnString = EntityUtils.toString(response.getEntity()); 
			                response = null; 
			         } else {
			        	 returnString = "Error";
			         }
			         
			     } catch (ClientProtocolException e) {
			      throw new PostRequestException(e.getMessage());
			     } catch (IOException e) {
			      throw new PostRequestException(e.getMessage());
			     }
			     
			     return returnString;
			 
			 }
			 
			 public static List<NameValuePair> composeParametersForPostRequest(Map<String,String> parameters){
			  
			  List<String> chiavi = new ArrayList<String>(parameters.keySet());
			  List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			  
			  for (int i = 0 ; i < chiavi.size(); i++){
			   String chiave = chiavi.get(i);
			   nameValuePairs.add(new BasicNameValuePair(chiave, parameters.get(chiave)));
			  }
			  
			  return nameValuePairs;
			 }
			 
			 /***
			  * Metodo per comporre la parte di URL relativa ai parametri
			  * @param parameters : mappa di parametri
			  * @return    : Stringa contenente i parametri concatenati
			  */
			 public static String composeParametersForGetRequest(Map<String, String> parameters){
			  String paramStr = "";
			  List<String> chiavi = new ArrayList<String>(parameters.keySet());
			  
			  for (int i = 0 ; i < chiavi.size(); i++){
			   String chiave = chiavi.get(i);
			   paramStr += chiave+"=";  
			   paramStr += URLEncoder.encode(parameters.get(chiave));  
			   paramStr += "&";  
			  }
			  return paramStr;
			 }
			}

	

