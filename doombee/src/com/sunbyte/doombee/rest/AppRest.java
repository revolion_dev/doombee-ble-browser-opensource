package com.sunbyte.doombee.rest;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;



//
//public class AppRest {
//
//	public static List<RestDictionary> RestSuffixes = new ArrayList<RestDictionary>();
//	private Context ctx;
//	private Activity activity;
//	public static SyncThread syncTaskRef;
//	public static List<TableStackItem> tstackItems;
//	
//	public static List<TableStackItem> tstackItemsUp;
//	
//	public AppRest(Context ctx, Activity activity) {
//		
//		this.ctx = ctx;
//		this.activity = activity;
//		
//		RestDictionary rd = null;
//		rd = new RestDictionary("exercises", "exercises", "ParseXMLExercises");
//		rd.setDAOClass("ExercisesDAO");
//		RestSuffixes.add(rd);
//		
//		rd = new RestDictionary("exercise_types", "exercisetypes", "ParseXMLExerciseTypes");
//		rd.setDAOClass("ExerciseTypesDAO");
//		RestSuffixes.add(rd);
//		
//		rd = new RestDictionary("groups", "groups", "ParseXMLGroups");
//		rd.setDAOClass("GroupsDAO");
//		RestSuffixes.add(rd);
//		
//		rd = new RestDictionary("training_plans", "trainingplans", "ParseXMLTrainingPlans");
//		rd.setDAOClass("TrainingPlansDAO");
//		RestSuffixes.add(rd);
//		
//		rd = new RestDictionary("training_types", "trainingtypes", "ParseXMLTrainingTypes");
//		rd.setDAOClass("TrainingTypesDAO");
//		RestSuffixes.add(rd);
//		
//		rd = new RestDictionary("next_trainings", "nexttrainings", "ParseXMLNextTraining");
//		rd.setDAOClass("NextTrainingsDAO");
//		RestSuffixes.add(rd);
//		// SENSOR SCREEN
//		
//		rd = new RestDictionary("pulse_exercises", "pulseexercises", "ParseXMLPulseExercises");
//		rd.setDAOClass("PulseExercisesDAO");
//		RestSuffixes.add(rd);
//		
//		rd = new RestDictionary("pulse_records", "pulserecords", "ParseXMLPulseRecords");
//		rd.setDAOClass("PulseRecordsDAO");
//		RestSuffixes.add(rd);
//		
//		rd = new RestDictionary("pulse_users", "pulseusers", "ParseXMLPulseUsers");
//		rd.setDAOClass("PulseUsersDAO");
//		RestSuffixes.add(rd);
//
//		// List of methods
//		// ParseXMLExerciseTypes
//		// ParseXMLExercises
//		// ParseXMLGroups
//		// ParseXMLTrainingTypes
//		// ParseXMLTrainingPlans
//	}
//	
//	public static void setTableStackItems(List<TableStackItem> tstackItems) {
//		AppRest.tstackItems = tstackItems;
//	}
//
//	
//	public void getTableStackItems() {
//		
//		DeviceManager devicemgr = new DeviceManager(this.ctx);
//		
//		if (devicemgr.isInternetAvailable())
//		{
//			String method = "get";
//			syncTaskRef = new SyncThread(this.activity, ctx, method, true, RestSuffixes);
//			syncTaskRef.execute();
//
//		} else {
//
//			Popups.ShowSimplePopup(this.activity, "No internet connection available.\nVerify your connection first!", "", "OK");
//			System.out.println("No Internet Available!");
//		}
//		
//	
//	}
//	
//	
//	public void saveTableStackItemsToUpStream(List<TableStackItem> data, List<RestDictionary> upSuffixes) {
//		
//		DeviceManager devicemgr = new DeviceManager(this.ctx);
//		
//		if (devicemgr.isInternetAvailable())
//		{
//			String method = "set";
//			syncTaskRef = new SyncThread(this.activity, method, data);
//			syncTaskRef.execute();
//			
//		} else {
//			Popups.ShowSimplePopup(this.ctx, "No internet connection available", "", "OK");
//			System.out.println("No Internet Available!");
//		}
//
//	}
//	
//}
