package com.sunbyte.doombee.rest;

public class RestDictionary {

	private String tableName;
	private String restSuffix;
	private String parseMethodName;
	private String DAOClass;
	private String DAOSetMethod;
	private String DAOGetMethod;
	
	public RestDictionary() {
	}
	
	public RestDictionary(String tableName, String restsuffix, String parseMethod) {
		this.tableName = tableName;
		this.restSuffix = restsuffix;
		this.parseMethodName = parseMethod;
	}
	
	public String getTableName() {
		return this.tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName; 
	}
	
	public String getRestSuffix() {
		return this.restSuffix;
	}
	
	public void setRestSuffix(String suffix) {
		this.restSuffix = suffix;
	}
	
	public void setRestDictionary(String tableName, String restsuffix) {
		this.tableName = tableName;
		this.restSuffix = restsuffix;
	}
	
	public void setParseMethod(String parseMethod) {
		this.parseMethodName = parseMethod;
	}
	
	public String getParseMethod() {
		return this.parseMethodName;
	}
	
	public void setDAOClass(String DAOClass) {
		this.DAOClass = DAOClass;
	}

	public void setDAOSetMethod(String DAOSetMethod) {
		this.DAOSetMethod = DAOSetMethod;
	}
	
	public void setDAOGetMethod(String DAOGetMethod) {
		this.DAOGetMethod = DAOGetMethod;
	}
	
	public String getDAOClass() {
		return this.DAOClass;
	}
	
	public String getDAOSetMethod() {
		return this.DAOSetMethod;
	}
	
	public String getDAOGetMethod() {
		return this.DAOGetMethod;
	}
}
