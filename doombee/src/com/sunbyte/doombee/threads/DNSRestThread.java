package com.sunbyte.doombee.threads;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sunbyte.doombee.globals.ApplicationStatus;
import com.sunbyte.doombee.rest.GetRequestException;
import com.sunbyte.doombee.rest.PostRequestException;
import com.sunbyte.doombee.rest.RestDictionary;
import com.sunbyte.doombee.rest.RestUtils;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;


public class DNSRestThread extends AsyncTask<Void, Void, Void> {
	
	
	private final String RESTBase =  ApplicationStatus.ServiceBase;
	public static ProgressDialog dialog;
	
	Activity ctx;
	Context ctx2;
	String method;
	Boolean fromStart;
	List<RestDictionary> suffixes;

	
	Boolean errorCheck;
	//boolean loginSuccess = false;
	
	public DNSRestThread(Activity parentActivity, Context ctx2, String method, Boolean dummy) {
		
		ctx = parentActivity;
		this.method = method;
		this.ctx2 = ctx2; 
		this.fromStart = dummy;
		this.errorCheck = false;
		this.dialog = null;
	}
	
	public DNSRestThread(Activity parentActivity, String method) {
		
		ctx = parentActivity;
		this.method = method;
		this.errorCheck = false;
		this.dialog = null;
	}

	@Override
	protected void onPreExecute() {
		if (this.method == "get") {
			if (dialog == null) {
				dialog = ProgressDialog.show(ctx, "", "Synchronizing...\nPlease wait up to 1 min.");
			}
		}
		if (this.method == "post") {
			if (dialog == null) {
				dialog = ProgressDialog.show(ctx, "", "Loading...\nUpload changes");
			}
		}
	}
	
	
	@Override
	protected Void doInBackground(Void... params) {

		String result = "";
		Map<String, String> paramx = new HashMap<String, String>();

		if (this.method == "get") {
			
			for (int i = 0; i < this.suffixes.size(); i++) {
				
				RestDictionary suffix = this.suffixes.get(i);
				
				try {
					result = RestUtils.doGETRequest(
							this.RESTBase + suffix.getRestSuffix(), paramx);
				} catch (GetRequestException e) {
					System.out.println("Connection error!");
					
					e.printStackTrace();
			    	dialog.dismiss();
			    	//Popups.ShowSimplePopup(ctx2, "Connection or server error!", "", "OK");
					this.errorCheck = true;
					return null;
				}
				
				
				if (result != "") {
					

				} else {
					System.out.println("GetTableStackItem: result empty");

					this.errorCheck = true;
			    	dialog.dismiss();
					return null;
				}
			
			}
			

			
		}
		
		
		
		if (this.method == "set") {
			
				
			try {
				result = RestUtils.doPOSTRequest(
						this.RESTBase, paramx);
			} catch (PostRequestException e) {
				//Popups.ShowSimplePopup(ctx, "Connection or server error!", "", "OK");
				System.out.println("Webservice: Error saving data!");
				this.errorCheck = true;
		    	dialog.dismiss();
				e.printStackTrace();
			}	
		}
		
		
		return null;
	}
	
	
	

    @Override
    protected void onPostExecute(Void unused) {
    	
    	if (this.fromStart != null) {
    	
	    	if (!this.errorCheck) {
	    		dialog.dismiss();
	    	} else {
	    		dialog.dismiss();
	    		//dialog = ProgressDialog.show(ctx, "", "An error occurred while synchronizing: Possibly no internet connection or server down!");
	
	    	}
    	} else {
	    	if (!this.errorCheck) {
	    		dialog.dismiss();
	    	
	    	} else {
	    		dialog.dismiss();
	    		//dialog = ProgressDialog.show(ctx, "", "An error occurred while synchronizing: Possibly no internet connection or server down!");
	
	    	}
    	}
//    	if (loginSuccess) {
//    		
//    		Credentials curCreds = new Credentials();
//    		curCreds.curUser = username;
//    		curCreds.curPswd = pass;
//    		ApplicationStatus.SetLoginStatus(true);
//    		ApplicationStatus.SetCurrentCred(curCreds);
//    		
//    		
//    		((Activity)ctx).finish();
//			
//    		Intent intent = new Intent(ctx, MainActivity.class);
//    		ctx.startActivity(intent);
//
//    	} else {
//    		
//    		// Password not valid
//    		Popups.ShowSimplePopup(ctx, "Username or password not valid!\nTry again!", "", "OK");
//    	}
    	

    }
	
	
}