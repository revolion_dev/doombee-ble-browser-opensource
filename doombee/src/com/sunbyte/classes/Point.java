package com.sunbyte.classes;

public class Point {
	float x;
	float y;
		
	public Point(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public float dist(Point p) {
		 return this.vect(p).norm();
	}
	
	public Point vect(Point p) {
		return new Point (p.x-this.x, p.y-this.y);
	}

	public float norm() {
		return (float)Math.sqrt (this.x*this.x+this.y*this.y);
	}

	public Point add(Point v) {
		return new Point (this.x + v.x, this.y + v.y);
	}
	
	public Point mult(float a) {
		return new Point (this.x * a, this.y * a);
	}

	
}
