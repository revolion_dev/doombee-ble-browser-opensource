package com.sunbyte.classes;

import java.util.ArrayList;

import kn.uni.voronoitreemap.j2d.Point2D;


import android.graphics.Canvas;
import android.graphics.Paint;


public class CirclePacking {
	
	  public ArrayList<Circle> nodes;
	  
	  public ArrayList<Point2D> circlePoints;
	  
	  int timesToPack = 10;
	  float width, height;
	  
	  public void ShowCircles(Canvas canvas, Paint p) {
		  
		  for (int i = 0; i < timesToPack; i++) {
			  this.draw(canvas, p);
		  }
	  }
	  
	  
	  public ArrayList<Point2D> getCirclePoints() {
		  
		  circlePoints = new ArrayList<Point2D>();
		  
		  for (int i = 0; i < nodes.size(); i++) {
			  Point2D newp = new Point2D(
					  				nodes.get(i).x, 
					  				nodes.get(i).y);
			  newp.weight = nodes.get(i).weight;
			  circlePoints.add( newp );
		  }
		  
		  return circlePoints;
	  }
	  
	  public CirclePacking(float _width, float _height) {
	    nodes = new ArrayList<Circle>();
		width = _width;
		height = _height;
		
	    
	  };
	  
	  public void ChangeRadiusesByWeight() {
		  
		  float percentReduction = 9;
		  // rect area
		  float rectArea = width * height;
		  //float averageNodeArea = rectArea / nodes.size();
		  int weightSum = 0;
		  for (int i = 0; i < nodes.size(); i++) {
			  weightSum += nodes.get(i).weight;
		  }
		  float areaPerWeight = rectArea / weightSum;
		  
		  
		  //int maxWeight = 10;
		  
		  for (int i = 0; i < nodes.size(); i++) {
			  Circle node = nodes.get(i);
			  float nodeR = node.r;
			  float nodeWeight = node.weight;
			  float newNodeArea = areaPerWeight * nodeWeight;
			  nodeR = (float) (Math.sqrt(newNodeArea) / 2); // 2.2 (Math.PI / 2)
			  
			  // reduces radius of fixed percentace to avoid algorithm freeze
			  nodeR -= nodeR * percentReduction / 100;

			  node.r = nodeR;
			  //System.out.println("RADIUS" + nodeR);
			  nodes.set(i, node); 
		  }
	  }

	  
	  public void packAllCircles() {
		  
		  boolean shouldRepeat = true;
		  int count = 0;
		  
		  while (shouldRepeat) {
			  
			  for (int i = 0; i < nodes.size(); i++) {
				  shouldRepeat = false;
				  
				  for (int j = 0; j < nodes.size(); j++) {
					  if (i == j) {
				          continue;
				      }
				        
				      boolean packresult = pack(nodes.get(i), nodes.get(j));
				      //System.out.println("PACK RESULT" + packresult);
				      if (packresult) {
				    	  shouldRepeat = true;
				      }
				   }
			  }
			  
			  count++;
			  if (count > timesToPack) break;
		  }
	  }
	  
	  public void draw(Canvas canvas, Paint p) {

		    for (int i = 0; i < nodes.size(); i++) {
		      for (int j = 0; j < nodes.size(); j++) {
		        if (i == j) {
		          continue;
		        }
		        
		        pack(nodes.get(i), nodes.get(j));
		      }
		      
		      Circle n = nodes.get(i);
		      n.draw(canvas, p);
		    }
	  };
	  
	  // circle pack
	  // if the radii intersect, push them apart
	  public boolean pack(Circle a, Circle b) {
		 
		 boolean intersected = false;
	     // screen bounds
	     if (a.x <= a.r) a.x = a.r;
	     if (a.y <= a.r) a.y = a.r;
	     
	     if (b.x <= b.r) b.x = b.r;
	     if (b.y <= b.r) b.y = b.r;
	     
	     if (a.x > (width - (a.r))) a.x = (width - (a.r));
	     if (a.y > (height - (a.r))) a.y = (height - (a.r));
	    	 
	    if (intersects(a, b)) {
	    	
	    	intersected = true;
 
		     float vx = a.x - b.x;
		     float vy = a.y - b.y;
		     float d = (vx * vx) + (vy * vy);
		      
		     float vmagnitude = (float)Math.sqrt((vx * vx) + (vy * vy));
		     vx = normalizeX(vx, vmagnitude);
		     vy = normalizeY(vy, vmagnitude);
		      

		     float m = (float)((a.r + b.r + 1 - Math.sqrt(d)) * 1.20);
		     vx = multX(vx, m);
		     vy = multY(vy, m);
		      
		     a.x += vx;
		     a.y += vy;
		      
		     b.x -= vx;
		     b.x -= vy;
	    	
	    }
	    
	     // screen bounds
	     if (a.x <= a.r) a.x = a.r;
	     if (a.y <= a.r) a.y = a.r;
	     
	     if (b.x <= b.r) b.x = b.r;
	     if (b.y <= b.r) b.y = b.r;
	     
	     if (a.x > (width - (a.r))) a.x = (width - (a.r));
	     if (a.y > (height - (a.r))) a.y = (height - (a.r ));
	    
	    return intersected;
	  };
	  
	  // vector
	  public float normalizeX(float vx, float vmagnitude) {
		  vx = vx / vmagnitude;
		  return vx;
	  };
	  
	  public float normalizeY(float vy, float vmagnitude) {
		  vy = vy / vmagnitude;
		  return vy;
	  };
	  
	  public float multX(float vx, float m) {
		  return vx *= m;
	  };
	  
	  public float multY(float vy, float m) {
		  return vy *= m;
	  };
	  
	  
	  public float distance(float ax, float ay, float bx, float by) {
	    float dx = ax - bx;
	    float dy = ay - by;
	 
	    return (float) Math.sqrt((dx*dx) + (dy*dy));
	  };
	 
	  public Boolean intersects(Circle a, Circle b) {
	    float d  = distance(a.x, a.y, b.x, b.y);
	    return (d < (a.r + b.r + 1));
	  };
	  

};

//public class CirclePacking {
//
//ArrayList<Float> circles;
//float[] list;
//float ratio;
//ArrayList<Circle> bounds;
//ArrayList<Circle> tmp_bounds;
//
//// deduce starting dimensions from surface
//float bounding_r; // "infinite" radius
//float w;
//float h;
//
//
//public CirclePacking(ArrayList<Float> circles, float ratio)
//{
//	this.circles = circles;
//	this.ratio   = ratio; //|| 1.0f;
//	this.list = this.solve();
//	
//	
//	
//}
//
//// return the corner placements for two circles
//ArrayList<Circle> corner (float radius, Circle c1, Circle c2)
//{
//	Point u = c1.c.vect(c2.c); // c1 to c2 vector
//	float A = u.norm();
//    if (A == 0) {
//    	ArrayList<Circle> retvalue = new ArrayList<Circle>();
//    	return retvalue; // same centers
//    }
//    u = u.mult(1/A); // c1 to c2 unary vector
//    // compute c1 and c2 intersection coordinates in (u,v) base
//    float B = c1.r+radius;
//    float C = c2.r+radius;
//    if (A > (B + C)) {
//    	ArrayList<Circle> retvalue = new ArrayList<Circle>();
//    	return retvalue; // too far apart
//    }
//    float x = (A + (B*B-C*C)/A)/2;
//    float y = (float) Math.sqrt (B*B - x*x);
//    Point base = c1.c.add (u.mult(x));
//    
//    ArrayList<Circle> res = new ArrayList<Circle>();
//    Point p1 = new Point (base.x -u.y * y, base.y + u.x * y);
//    Point p2 = new Point (base.x +u.y * y, base.y - u.x * y);
//    if (in_rect(radius, p1)) res.add(new Circle (radius, p1));
//    if (in_rect(radius, p2)) res.add(new Circle (radius, p2));
//    return res;
//}
//
//
//public ArrayList<Circle> compute(float surface) {
//	bounding_r = (float)Math.sqrt(surface) * 100;
//	this.w = (float)Math.sqrt (surface * this.ratio);
//	this.h = this.w / this.ratio;
//	
//	// place our bounding circles
//    ArrayList<Circle> placed = new ArrayList<Circle>();
//    placed.add(bounding_circle ( 1,  1,  1, -1));
//    placed.add(bounding_circle ( 1, -1, -1, -1));
//    placed.add(bounding_circle (-1, -1, -1,  1));
//    placed.add(bounding_circle (-1,  1,  1,  1));
//   
//     // Initialize our rectangles list
//     @SuppressWarnings("unchecked")
//	ArrayList<Float> unplaced = (ArrayList<Float>) this.circles.clone();
//     
//
//     while (unplaced.size() > 0)
//     {
//         // compute all possible placements of the unplaced circles
//         double[] lambda = new double[unplaced.size()];
//         ArrayList<Circle> circle = new ArrayList<Circle>();
//         
//         for (int i = 0 ; i != unplaced.size() ; i++)
//         {
//             double lambda_min = 1e10;
//             lambda[i] = -1e10;
//             // match current circle against all possible pairs of placed circles
//             for (int j = 0   ; j < placed.size(); j++)
//             for (int k = j+1 ; k < placed.size(); k++)
//             {
//                 // find corner placement
//                 if (k > 3) {
//                	int zog=1;
//                 }
//                 ArrayList<Circle> corners = corner (unplaced.get(i), placed.get(j), placed.get(k));
// 
//                 // check each placement
//                 for (int c = 0 ; c != corners.size(); c++)
//                 {
//                     // check for overlap and compute min distance
//                     double d_min = 1e10;
//                     
//                     int l = 0;
//                     for (l = 0 ; l != placed.size(); l++)
//                     {
//                         // skip the two circles used for the placement
//                         if (l==j || l==k) continue;
//                                             
//                         // compute distance from current circle
//                         float d = placed.get(l).distance(corners.get(c));
//                         if (d < 0) break; // circles overlap
//                         
//                         if (d < d_min) d_min = d;
//                     }
//                     if (l == placed.size()) // no overlap
//                     {
//                         if (d_min < lambda_min)
//                         {
//                             lambda_min = d_min;
//                             lambda[i] = 1- d_min/unplaced.get(i);
//                             // maybe add before element
//                             circle.set(i, corners.get(c));
//                         }
//                     }
//                 }
//             }
//         }
//         
//         // select the circle with maximal gain
//         double lambda_max = -1e10;
//         int i_max = -1;
//         for (int i = 0 ; i != unplaced.length ; i++)
//         {
//             if (lambda[i] > lambda_max)
//             {
//                 lambda_max = lambda[i];
//                 i_max = i;
//             }
//         }
//         
//         // failure if no circle fits
//         if (i_max == -1) break;
//         
//         // place the selected circle
//         unplaced.remove(i_max);
//         placed.add(circle.get(i_max));
//     }
//     
//     // return all placed circles except the four bounding circles
//     this.tmp_bounds.remove(0);
//     this.tmp_bounds.remove(1);
//     this.tmp_bounds.remove(2);
//     this.tmp_bounds.remove(3);
//     return placed;
//}
//
//// approximate a segment with an "infinite" radius circle
//public Circle bounding_circle( float x0, float y0, float x1, float y1)
//{
//    float xm = Math.abs ((x1-x0)*w);
//    float ym = Math.abs ((y1-y0)*h);
//    float m = xm > ym ? xm : ym;
//    float theta = (float)Math.asin(m/4/bounding_r);
//    float r = (float)(bounding_r * Math.cos (theta));
//    return new Circle (bounding_r, 
//        new Point (r*(y0-y1)/2+(x0+x1)*w/4, 
//                   r*(x1-x0)/2+(y0+y1)*h/4));
//}
//
//
//// check if a circle is inside our rectangle
//public boolean in_rect (float radius, Point center)
//{
//    if (center.x - radius < - w/2) return false;
//    if (center.x + radius >   w/2) return false;
//    if (center.y - radius < - h/2) return false;
//    if (center.y + radius >   h/2) return false;
//    return true;
//}
//
//
//public float[] solve()
//{
//    // compute total surface of the circles
//    float surface = 0;
//    for (int i = 0 ; i != this.circles.length ; i++)
//    {
//        surface += Math.PI * Math.pow(this.circles[i],2);
//    }
//    
//    // set a suitable precision
//    float limit = surface/1000;
//    
//    float step = surface/2;
//    float[] res;
//    while (step > limit)
//    {
//        float[] placement = this.compute(surface);
//        System.out.println("placed" + placement.length+"out of"+this.circles.length+"for surface"+ surface);
//        
//		if (placement.length != this.circles.length)
//        {
//            surface += step;
//        }
//        else
//        {
//            res = placement;
//            this.bounds = this.tmp_bounds;
//            surface -= step;
//        }
//        step /= 2;
//    }
//    return res; 
//}
//
//
//
//
//
//
//compute: function (surface)
//{
//
//
//
// 
//
//
// /////////////////////////////////////////////////////////////////
// 
//
// 
// 
//},
//
//// find the smallest rectangle to fit all circles
//solve: function ()
//{
// // compute total surface of the circles
// var surface = 0;
// for (var i = 0 ; i != this.circles.length ; i++)
// {
//     surface += Math.PI * Math.pow(this.circles[i],2);
// }
// 
// // set a suitable precision
// var limit = surface/1000;
// 
// var step = surface/2;
// var res = [];
// while (step > limit)
// {
//     var placement = this.compute.call (this, surface);
//console.log ("placed",placement.length,"out of",this.circles.length,"for surface", surface);
//     if (placement.length != this.circles.length)
//     {
//         surface += step;
//     }
//     else
//     {
//         res = placement;
//         this.bounds = this.tmp_bounds;
//         surface -= step;
//     }
//     step /= 2;
// }
// return res; 
//}
//};
//
////====
////demo
////====
//function draw_result (packer)
//{
//function draw_circle (circle)
//{
// ctx.beginPath();
// ctx.arc ((circle.c.x+dx)*zoom+mx, (circle.c.y+dy)*zoom+my, circle.r*zoom, 0, 2*Math.PI);
// ctx.closePath();
// ctx.stroke();
//}
//
//var canvas = document.getElementById ('canvas');
//var ctx = canvas.getContext("2d");
//canvas.width +=0; // clear canvas
//var margin_factor = 0.1;
//
//var mx = canvas.width * margin_factor / 2;
//var my = canvas.height* margin_factor / 2;
//var dx = packer.w/2;
//var dy = packer.h/2;
//var zx = canvas.width  * (1-margin_factor) / packer.w;
//var zy = canvas.height * (1-margin_factor) / packer.h;
//var zoom = zx < zy ? zx : zy;
//
//// draw all circles
//ctx.strokeStyle = 'black';
//for (var i = 0 ; i != packer.list.length ; i++)
// draw_circle (packer.list[i]);
// 
//// draw bounding circles
//ctx.strokeStyle = 'red';
//for (var i = 0 ; i != packer.bounds.length ; i++)
// draw_circle (packer.bounds[i]);
// 
//// draw rectangle
//ctx.strokeStyle = 'orange';
//ctx.beginPath();
//ctx.rect((-packer.w/2+dx)*zoom+mx, (-packer.h/2+dy)*zoom+my, packer.w*zoom, packer.h*zoom);
//ctx.closePath();
//ctx.stroke();
//}
//
//function draw ()
//{
//var circles = parseInt  (document.getElementById('c').value);
//var ratio   = parseFloat(document.getElementById('r').value);
//var min_r   = parseInt  (document.getElementById('a').value);
//var max_r   = parseInt  (document.getElementById('b').value);
//var radiuses = [];
//for (var i = 0 ; i != circles ; i++)
// radiuses.push (Math.random() * (max_r-min_r) + min_r);
//var packer = new Packer (radiuses, ratio);
//draw_result(packer);    
//}


	
//public class CirclePacking {
//
//	  float width, height, padding, xcenter, ycenter;
//	  public ArrayList<Circle> circles;
//	  float damping, iterations;
//
//	  public CirclePacking(float _width, float _height)
//	  {
//	    width = _width;
//	    height = _height;
//	    xcenter = width/2;
//	    ycenter = height/2;
//	    circles = new ArrayList<Circle>();
//	    padding = 10.0f;
//	    damping = 0.01f;
//	    iterations = 1;
//	  }
//
//	  float fast_distance(float _x1, float _y1, float _x2, float _y2)
//	  {
//	    return (_x1 - _x2) * (_x1 - _x2) + (_y1 - _y2) * (_y1 - _y2);
//	  }
//
//	  public void pack()
//	  {
//
//	    for (int i = 0; i < circles.size(); i++)
//	    {
//	      Circle c1 = (Circle) circles.get(i);
//
//	      for (int j = i+1; j < circles.size(); j++)
//	      {
//	        Circle c2 = (Circle) circles.get(j);
//
//	        float d = fast_distance(c1.x, c1.y, c2.x, c2.y);
//	        float r = c1.r + c2.r + padding;
//
//	        if (d < (r*r))
//	        {
//	          float dx = c2.x - c1.x;
//	          float dy = c2.y - c1.y;
//	          float droot = (float) Math.sqrt(d);
//
//	          // proviamo a dare un peso rispetto al centro
//	          float cd1 = fast_distance(c1.x, c1.y, xcenter, ycenter);
//	          float cd2 = fast_distance(c1.x, c1.y, xcenter, ycenter);
//
//	          float total = dx + dy;
//
//	          float vx = (dx/droot) * (r-droot);
//	          float vy = (dy/droot) * (r-droot);
//
//	          c1.x -= vx * cd1/(cd1+cd2);
//	          c1.y -= vy * cd1/(cd1+cd2);
//	          c2.x += vx * cd2/(cd1+cd2);
//	          c2.y += vy * cd2/(cd1+cd2);
//	        }
//	      }
//	    }
//
//	    // contraction...
//	    //
//	    for (int i = 0; i < circles.size(); i++)
//	    {
//	      Circle c = (Circle) circles.get(i);
//	      float vx = (c.x - xcenter) * damping;
//	      float vy = (c.y - ycenter) * damping;
//	      c.x -= vx;
//	      c.y -= vy;
//	    }
//	    //
//	  }
//
//	  public void update() {
//	    for (int w=0; w<iterations; w++)
//	    {
//	      this.pack();
//	    }
//	  }
//	  /**
//	   * Draw all the circles
//	   */
//
//	  public void draw(Canvas canvas, Paint p)
//	  {
//	    for (int i = 0; i < circles.size(); i++)
//	    {
//	      Circle c = (Circle) circles.get(i);
//	      if (c.r < 1)
//	      {
//	        circles.remove(c);
//	      }
//	      else
//	      {
//	        c.draw(canvas, p);
//	      }
//	    }
//	  }
//	}