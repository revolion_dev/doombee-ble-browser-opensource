<?php

include '../main/index.php';

class TrainingPlans {
    private $db;
	private $rest;
	
	const DB_prefix = "app_";
	const DB_TABLE = "training_plans";
	
	const EntityName = "training_plan";
	
	private static $TABLE_ARRAY_STRUCT = array(
			"COLUMN_TRAINING_PLAN_ID" => "training_plan_id",
			"COLUMN_TRAINING_TYPES_ID" => "training_types_id",
			"COLUMN_GROUPS_ID" => "groups_id",
			"COLUMN_SEQUENCE" => "sequence",
			"COLUMN_EXERCISES_ID" => "exercises_id",
			"COLUMN_EXERCISE_TYPE_ID_1" => "exercise_type_id_1",
			"COLUMN_EXERCISE_TYPE_ID_2" => "exercise_type_id_2",
			"COLUMN_EXERCISE_TYPE_ID_3" => "exercise_type_id_3",
			"COLUMN_EXERCISE_TYPE_ID_4" => "exercise_type_id_4",
			"COLUMN_TIME" => "time",
			"COLUMN_SPLIT_TIME" => "split_time",
			"COLUMN_COMMENT" => "comment",
			"COLUMN_LAST_MODIFIED" => "last_modified",
			"COLUMN_DELETED" => "del_check"
	);
	private static $TABLE_ARRAY_TYPES = array('i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 's', 's', 'i');


		      																		
	public function setDb($db) {
		$this->db = $db;
	}
	
	public function setRest($rest) {
		$this->rest = $rest;
	}
	
	

	public function getTrainingPlans() {
	
		$curTable = self::DB_prefix . self::DB_TABLE;
		$theTable = self::DB_TABLE;
		$stmt = $this->db->prepare('SELECT * FROM ' . $curTable);
		//$stmt->bind_param("sii", $uid, $limitStart, $limit);
		$stmt->execute();
		$stmt->bind_result($training_plan_id, $trainingtype_id, $groups_id, $sequence, $exercises_id, 
				$extypeId1, $extypeId2, $extypeId3, $extypeId4, $time, $splitTime, $comment, $lastModified, $deleted);
	
		$cnt = 0;
		$result = null;
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		while ($stmt->fetch()) {
			$cnt++;
			$result["$theTable"][] = array(
				self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[0]"] => $training_plan_id,
				self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[1]"] => $trainingtype_id,
				self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[2]"] => $groups_id,
				self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[3]"] => $sequence,
				self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[4]"] => $exercises_id,
				self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[5]"] => $extypeId1,
				self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[6]"] => $extypeId2,
				self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[7]"] => $extypeId3,
				self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[8]"] => $extypeId4,
				self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[9]"] => $time,
				self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[10]"] => $splitTime,
				self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[11]"] => $comment,
				self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[12]"] => $lastModified,
				self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[13]"] => $deleted
			);

		}
		$stmt->close();
	
		return $result;
	}
	
	public function updateTrainingPlans($XmlObjectData, $Ids) {
		
		$error = false;
		$qArray = array();
		$bindParam = new BindParam();
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		$idsArr = array();
		$idsArr = explode(',', $Ids);
		
		// get array from object
		$xml = json_decode(json_encode((array) $XmlObjectData), 1); // convert to array
		
		// adjusting array if only one element is present
		if (!is_array($xml[self::EntityName][0])) {
			if (is_array($xml[self::EntityName])) {
				$first = $xml[self::EntityName];
				$xml[self::EntityName] = array();
				$xml[self::EntityName][] = $first;
			}
		}
		

		$cnt = 0;
		foreach($xml[self::EntityName] as $trainingplan) {
			
			$query = "";
			$bindParam = new BindParam();
			$qArray = array();
			
			for ($i = 0; $i < count(self::$TABLE_ARRAY_STRUCT); $i++) {
		
				$key = self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[$i]"];
				$qArray[] = $key . ' = ?';
				$value = $trainingplan["$key"];
				if (is_array($value)) {
					$value = "";
				}
				
				$type = self::$TABLE_ARRAY_TYPES[$i];
				$bindParam->add($type, $value);
			}
				
			$query .= implode(', ', $qArray);
		
			$stmt = $this->db->prepare("UPDATE " . self::DB_prefix . self::DB_TABLE . " SET " . $query . " WHERE " .  self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[0]"] . " = " . $idsArr[$cnt]);
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
				
			call_user_func_array( array($stmt, 'bind_param'), $bindParam->get());
			$stmt->execute();
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
			$cnt++;
		}
		
		$stmt->close();
		
		if ($error) {
			return null;
		}
		return 1;
	}
	
	public function deleteTrainingPlans($Ids) {
	
		$idsArr = array();
		$idsArr = explode(',', $Ids);
		
		$mysqli = $this->db;
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		foreach ($idsArr as $id) {
			$stmt = $this->db->prepare("DELETE FROM `" . self::DB_prefix . self::DB_TABLE . "` WHERE `" . self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[0]"] . "` = ?");
			$stmt->bind_param('i', $id);
			$stmt->execute();
			$stmt->close();
		}
	}
	
	public function saveTrainingPlans($XmlObjectData) {
		
		$error = false;
		$bindParam = new BindParam();
		$qArray = array();
		$vArray = array();
		$resultSql = "";
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		// get array from object
		$xml = json_decode(json_encode((array) $XmlObjectData), 1); // convert to array

		// adjusting array if only one element is present
		if (!is_array($xml[self::EntityName][0])) {
			if (is_array($xml[self::EntityName])) {
				$first = $xml[self::EntityName];
				$xml[self::EntityName] = array();
				$xml[self::EntityName][] = $first;
			}
		}
		
		foreach($xml[self::EntityName] as $trainingplan) {
			$query = "";
			$values = "";
			$bindParam = new BindParam();
			$qArray = array();
			$vArray = array();
			
			for ($i = 0; $i < count(self::$TABLE_ARRAY_STRUCT); $i++) {
				$key = self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[$i]"];
				$qArray[] = $key;
				$vArray[] = '?';
				
				$value = $trainingplan["$key"];
				if (is_array($value)) {
					$value = "";
				}
				$type = self::$TABLE_ARRAY_TYPES[$i];
				
				echo "value" . $value;
				$bindParam->add($type, $value);
			}
				
			$query .= implode(', ', $qArray);
			$values .= implode(', ', $vArray);
			
			$resultSql = "INSERT INTO " . self::DB_prefix . self::DB_TABLE . " (" . $query . ") VALUES (" . $values . ");";
			
			echo $resultSql;
			$stmt = $this->db->prepare($resultSql);
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
				
			call_user_func_array( array($stmt, 'bind_param'), $bindParam->get());
			$stmt->execute();
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
		
		}
		
		$stmt->close();
		
		if ($error) {
			return null;
		}
		return 1;
	}
 
	
	
    // Main method 
    function handleTrainingPlansRequests($requestMethod) {
		

        // /webservice/trainingplans/  GET all trainingplans
    	if (strcmp($requestMethod, 'GET') == 0) {
    		$method = $_GET["method"];
    		 
    		if (strcmp($method, 'delete') != 0) {
    			 
    			$result = $this->getTrainingPlans();
    	
    			if ($result != null) {
    				$this->rest->sendResponse(200, $this->rest->xml_encode($result), 'application/xml');
    				return true;
    			} else {
    				$this->rest->sendResponse(200, $this->rest->xml_encode(""));
    				return false;
    			}
    	
    		} else {
    	
    			$Ids = $_GET["Ids"];
    	
    			if ($Ids != null) {
    	
    				$ret = $this->deleteTrainingPlans($Ids);
    				$this->rest->sendResponse(200, $ret);
    			}
    		}
    		 
    		 
    	}
    	
    	// /webservice/exercises/
    	// POST (data contains XML structure) save all exercises
    	
    	if (strcmp($requestMethod, 'POST') == 0) {
    			
    		$method = $_POST["method"];

    		if (strcmp($method, 'update') != 0) {
    				
    			$data = null;
    			$data = $_POST["data"];
    			if ($data != null) {
    				$XmlObjectData = $this->rest->xml_decode($data);
    				$ret = $this->saveTrainingPlans($XmlObjectData);
    					
    				if ($ret != null) {
    					$this->rest->sendResponse(200, $ret);
    					return true;
    				} else {
    					$this->rest->sendResponse(500, "Unable to save data!");
    					return false;
    				}
    	
    			}
    				
    		} else {

    			$data = null;
    			$data = $_POST["data"];
    			$Ids = $_POST["Ids"];
    				
    			if ($data != null) {
    				$XmlObjectData = $this->rest->xml_decode($data);
    				
    				$ret = $this->updateTrainingPlans($XmlObjectData, $Ids);
    				
    				if ($ret != null) {
    					$this->rest->sendResponse(200, $ret);
    					return true;
    				} else {
    					$this->rest->sendResponse(500, "Unable to update data!");
    					return false;
    				}
    	
    			}
    		}
    			
    	}
    	
/*     	if ($requestMethod=='GET') {
    		$result = $this->getTrainingPlans();
    		 
    		if ($result != null) {
    			$this->rest->sendResponse(200, $this->rest->xml_encode($result), 'application/xml');
    			return true;
    		} else {
    			$this->rest->sendResponse(200, $this->rest->xml_encode(""));
    			return false;
    		}
    	}
    	 
    	// /webservice/trainingplans/
    	// POST (data contains XML structure) save all trainingplans
    	 
    	if ($requestMethod=='POST') {
    		 
    		$data = null;
    		$data = $_POST["data"];
    		if ($data != null) {
    			$XmlObjectData = $this->rest->xml_decode($data);
    			$ret = $this->saveTrainingPlans($XmlObjectData);
    			 
    			if ($ret != null) {
    				$this->rest->sendResponse(200, $ret);
    				return true;
    			} else {
    				$this->rest->sendResponse(500, "Unable to save data!");
    				return false;
    			}
    	
    		}
    	}
    	 */
		
	}
}


$api = new MasterSport;
$trainingPlans = new TrainingPlans;
$trainingPlans->setDb($api->db);
$trainingPlans->setRest($api->rest);
$trainingPlans->handleTrainingPlansRequests($_SERVER['REQUEST_METHOD']);
?>

