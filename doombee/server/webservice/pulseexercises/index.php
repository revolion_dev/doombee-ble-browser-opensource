﻿<?php

include '../main/index.php';


class PulseExercises {
	
    private $db;
	private $rest;
	
	const DB_prefix = "app_";
	const DB_TABLE = "pulse_exercises";
	
	const EntityName = "pulse_exercise";
	
	private static $TABLE_ARRAY_STRUCT = array(
			"COLUMN_ID" => "ID",
			"COLUMN_NAME" => "name",
			"COLUMN_LAST_MODIFIED" => "last_modified",
			"COLUMN_DELETED" => "del_check"
			);
	private static $TABLE_ARRAY_TYPES = array('i', 's', 's', 's');
	
	
	
	public function setDb($db) {
		$this->db = $db;	
	}
 
	public function setRest($rest) {
		$this->rest = $rest;
	}
	
	
	public function getPulseExercises() {

		$curTable = self::DB_prefix . self::DB_TABLE;
		$theTable = self::DB_TABLE;
		
		$stmt = $this->db->prepare('SELECT * FROM ' . $curTable);
		//$stmt->bind_param("sii", $uid, $limitStart, $limit);
		$stmt->execute();
		$stmt->bind_result($id, $name, $lastmodified, $deleted);
		
		$cnt = 0;
		$result = null;
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		while ($stmt->fetch()) {
			$cnt++;
			$result["$theTable"][] = array(
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[0]"] => $id,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[1]"] => $name,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[2]"] => $lastmodified,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[3]"] => $deleted
			);
		}
		$stmt->close();
		
		return $result;
	}
	
	
	public function updatePulseExercises($XmlObjectData, $Ids) {
		
		$error = false;
		$qArray = array();
		$vArray = array();
		$bindParam = new BindParam();
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		$idsArr = array();
		$idsArr = explode(',', $Ids);

		// get array from object
		$xml = json_decode(json_encode((array) $XmlObjectData), 1); // convert to array

		$cnt = 0;
		foreach($xml as $trainingplan) {

			for ($i = 0; $i < count(self::$TABLE_ARRAY_STRUCT); $i++) {
				
				$key = self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[$i]"];
				$qArray[] = $key . ' = ?';
				$value = $trainingplan["$key"];
				if (is_array($value)) {
					$value = "";
				}
				$type = self::$TABLE_ARRAY_TYPES[$i];
				$bindParam->add($type, $value);
			}
			
			$query .= implode(', ', $qArray);

			$stmt = $this->db->prepare("UPDATE " . self::DB_prefix . self::DB_TABLE . " SET " . $query . " WHERE " .  self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[0]"] . " = " . $idsArr[$cnt]);
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
			
			call_user_func_array( array($stmt, 'bind_param'), $bindParam->get());
			$stmt->execute();
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
			$cnt++;
		}
		
		$stmt->close();
		
		if ($error) {
			return null;
		}
		return 1;
	}
	
	public function deletePulseExercises($Ids) {
		
		$idsArr = array();
		$idsArr = explode(',', $Ids);
		
		$mysqli = $this->db;
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		foreach ($idsArr as $id) {
			$stmt = $this->db->prepare("DELETE FROM `" . self::DB_prefix . self::DB_TABLE . "` WHERE `" . self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[0]"] . "` = ?");
			$stmt->bind_param('i', $id);
			$stmt->execute();
			$stmt->close();
		}
	}
	
	public function savePulseExercises($XmlObjectData) {
		
		$error = false;
		$qArray = array();
		$vArray = array();
		$bindParam = new BindParam();
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		// get array from object
		$xml = json_decode(json_encode((array) $XmlObjectData), 1); // convert to array
		
		foreach($xml as $pulseexercise) {
			for ($i = 0; $i < count(self::$TABLE_ARRAY_STRUCT); $i++) {
				$key = self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[$i]"];
				$qArray[] = $key;
				$vArray[] = '?';
				$value = (string) $pulseexercise["$key"];
				$type = self::$TABLE_ARRAY_TYPES[$i];
				$bindParam->add($type, $value);
			}
		
			$query .= implode(', ', $qArray);
			$values .= implode(', ', $vArray);
		
			$stmt = $this->db->prepare("INSERT INTO " . self::DB_prefix . self::DB_TABLE . " (" . $query . ") VALUES (" . $values . ")");
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
				
			call_user_func_array( array($stm, 'bind_param'), $bindParam->get());
			$stmt->execute();
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
			//$affRows = $affRows + $stmt->rowCount();
		}
		
		
		$stmt->close();
		
		if ($error) {
			return null;
		}
		return 1;
	}
	
	
    // Main method 
    function handlePulseExercisesRequests($requestMethod) {
		
    	
    	if (strcmp($requestMethod, 'GET') == 0) {
    		$method = $_GET["method"];
    		 
    		if (strcmp($method, 'delete') != 0) {
    			 
    			$result = $this->getPulseExercises();
    	
    			if ($result != null) {
    				$this->rest->sendResponse(200, $this->rest->xml_encode($result), 'application/xml');
    				return true;
    			} else {
    				$this->rest->sendResponse(200, $this->rest->xml_encode(""));
    				return false;
    			}
    	
    		} else {
    	
    			$Ids = $_GET["Ids"];
    	
    			if ($Ids != null) {
    	
    				$ret = $this->deletePulseExercises($Ids);
    				$this->rest->sendResponse(200, $ret);
    			}
    		}
    		 
    		 
    	}
    	
    	// /webservice/exercises/
    	// POST (data contains XML structure) save all exercises
    	
    	if (strcmp($requestMethod, 'POST') == 0) {
    			
    		$method = $_POST["method"];
    			
    		if (strcmp($method, 'update') != 0) {
    				
    			$data = null;
    			$data = $_POST["data"];
    			if ($data != null) {
    				$XmlObjectData = $this->rest->xml_decode($data);
    				$ret = $this->savePulseExercises($XmlObjectData);
    					
    				if ($ret != null) {
    					$this->rest->sendResponse(200, $ret);
    					return true;
    				} else {
    					$this->rest->sendResponse(500, "Unable to save data!");
    					return false;
    				}
    	
    			}
    				
    		} else {
    	
    			$data = null;
    			$data = $_POST["data"];
    			$Ids = $_POST["Ids"];
    				
    			if ($data != null) {
    				$XmlObjectData = $this->rest->xml_decode($data);
    				$ret = $this->updatePulseExercises($XmlObjectData, $Ids);
    	
    				if ($ret != null) {
    					$this->rest->sendResponse(200, $ret);
    					return true;
    				} else {
    					$this->rest->sendResponse(500, "Unable to update data!");
    					return false;
    				}
    	
    			}
    		}
    			
    	}
    	
    	
    	// /webservice/exercises/  GET all exercises
/*         if ($requestMethod=='GET') {
        	$result = $this->getPulseExercises();
        	
			if ($result != null) {
				$this->rest->sendResponse(200, $this->rest->xml_encode($result), 'application/xml');
				return true;	
			} else {
				$this->rest->sendResponse(200, $this->rest->xml_encode(""));
				return false;
			}
		}
		
		// /webservice/exercises/  
		// POST (data contains XML structure) save all exercises
		
		if ($requestMethod=='POST') {
			
			$data = null;
			$data = $_POST["data"];
			if ($data != null) {
				$XmlObjectData = $this->rest->xml_decode($data);
				$ret = $this->savePulseExercises($XmlObjectData);
				
				if ($ret != null) {
					$this->rest->sendResponse(200, $ret);
					return true;
				} else {
					$this->rest->sendResponse(500, "Unable to save data!");
					return false;
				} 
					
			}		
		}

		if ($requestMethod == 'PUT') {
			
			$data = null;
			$data = $_POST["data"];
			$Ids = $_POST["Ids"];
			
			if ($data != null) {
				$XmlObjectData = $this->rest->xml_decode($data);
				$ret = $this->updatePulseExercises($XmlObjectData, $Ids);
			
				if ($ret != null) {
					$this->rest->sendResponse(200, $ret);
					return true;
				} else {
					$this->rest->sendResponse(500, "Unable to update data!");
					return false;
				}
					
			}
		}
		
		if ($requestMethod == 'DELETE') {
				
			$Ids = $_POST["Ids"];
				
			if ($Ids != null) {

				$ret = $this->deletePulseExercises($Ids);
				$this->rest->sendResponse(200, $ret);
			}
		} */
	}

}


$api = new MasterSport;
$pulseExercises = new PulseExercises;
$pulseExercises->setDb($api->db);
$pulseExercises->setRest($api->rest);
$pulseExercises->handlePulseExercisesRequests($_SERVER['REQUEST_METHOD']);
?>