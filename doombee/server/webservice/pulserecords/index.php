﻿<?php

include '../main/index.php';


class PulseRecords {
	
    private $db;
	private $rest;
	
	const DB_prefix = "app_";
	const DB_TABLE = "pulse_records";											

	const EntityName = "pulse_record";
	
	private static $TABLE_ARRAY_STRUCT = array(
			"COLUMN_ID" => "ID",
			"COLUMN_PULSE_USER_ID" => "pulse_user_id",
			"COLUMN_PULSE_EXERCISE_ID" => "pulse_exercise_id",
			"COLUMN_DATA" => "data",
			"COLUMN_DATAFILE" => "data_file",
			"COLUMN_DATEOFRECORD" => "record_date",
			"COLUMN_PULSEAVERAGE_FOR_RECORD" => "record_pulse_average",
			"COLUMN_PULSEMAX_FOR_RECORD" => "record_pulse_max",
			"COLUMN_PULSESTART_FOR_RECORD" => "record_pulse_start",
			"COLUMN_PULSERECOVERY_FOR_RECORD" => "record_pulse_recovery",
			"COLUMN_COMMENTS" => "record_comment",
			"COLUMN_LAST_MODIFIED" => "last_modified",
			"COLUMN_DELETED" => "del_check"
			);
	private static $TABLE_ARRAY_TYPES = array('i', 'i', 'i', 's', 's', 's', 'i', 'i', 'i', 'i', 's', 's', 's');
	
	
	
	public function setDb($db) {
		$this->db = $db;	
	}
 
	public function setRest($rest) {
		$this->rest = $rest;
	}
	
	
	public function getPulseRecords() {

		$curTable = self::DB_prefix . self::DB_TABLE;
		$theTable = self::DB_TABLE;
		
		$stmt = $this->db->prepare('SELECT * FROM ' . $curTable);
		//$stmt->bind_param("sii", $uid, $limitStart, $limit);
		$stmt->execute();
		$stmt->bind_result($id, $pulseUserId, $pulseExerciseId, $data, $datafile, $dateofrecord, 
				$pulseAverage, $pulseMax, $pulseStart, $pulseRecovery, $comments, $lastmodified, $deleted);
		
		$cnt = 0;
		$result = null;
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		while ($stmt->fetch()) {
			$cnt++;
			$result["$theTable"][] = array(
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[0]"] => $id,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[1]"] => $pulseUserId,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[2]"] => $pulseExerciseId,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[3]"] => $data,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[4]"] => $datafile,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[5]"] => $dateofrecord,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[6]"] => $pulseAverage,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[7]"] => $pulseMax,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[8]"] => $pulseStart,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[9]"] => $pulseRecovery,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[10]"] => $comments,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[11]"] => $lastmodified,
					self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[12]"] => $deleted
			);
		}
		$stmt->close();
		
		return $result;
	}
	
	
	public function updatePulseRecords($XmlObjectData, $Ids) {
		
		$error = false;
		$qArray = array();
		$bindParam = new BindParam();
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		$idsArr = array();
		$idsArr = explode(',', $Ids);
		
		// get array from object
		$xml = json_decode(json_encode((array) $XmlObjectData), 1); // convert to array
		
		// adjusting array if only one element is present
		if (!is_array($xml[self::EntityName][0])) {
			if (is_array($xml[self::EntityName])) {
				$first = $xml[self::EntityName];
				$xml[self::EntityName] = array();
				$xml[self::EntityName][] = $first;
			}
		}
		
		$cnt = 0;
		foreach($xml[self::EntityName] as $pulserecord) {
			
			$query = "";
			$bindParam = new BindParam();
			$qArray = array();
			
			for ($i = 0; $i < count(self::$TABLE_ARRAY_STRUCT); $i++) {
		
				$key = self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[$i]"];
				$qArray[] = $key . ' = ?';
				$value = $pulserecord["$key"];
				if (is_array($value)) {
					$value = "";
				}
				$type = self::$TABLE_ARRAY_TYPES[$i];
				$bindParam->add($type, $value);
			}
				
			$query .= implode(', ', $qArray);
		
			$stmt = $this->db->prepare("UPDATE " . self::DB_prefix . self::DB_TABLE . " SET " . $query . " WHERE " .  self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[0]"] . " = " . $idsArr[$cnt]);
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
				
			call_user_func_array( array($stmt, 'bind_param'), $bindParam->get());
			$stmt->execute();
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
			$cnt++;
		}
		
		$stmt->close();
		
		if ($error) {
			return null;
		}
		return 1;
	}
	
	public function deletePulseRecords($Ids) {
		
		$idsArr = array();
		$idsArr = explode(',', $Ids);
		
		$mysqli = $this->db;
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		foreach ($idsArr as $id) {
			$stmt = $this->db->prepare("DELETE FROM `" . self::DB_prefix . self::DB_TABLE . "` WHERE `" . self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[0]"] . "` = ?");
			$stmt->bind_param('i', $id);
			$stmt->execute();
			$stmt->close();
		}
	}
	

	public function savePulseRecords($XmlObjectData) {
		
		$error = false;
		$bindParam = new BindParam();
		$qArray = array();
		$vArray = array();
		$resultSql = "";
		$Table_Array_Keys = array_keys(self::$TABLE_ARRAY_STRUCT);
		
		// get array from object
		$xml = json_decode(json_encode((array) $XmlObjectData), 1); // convert to array

		// adjusting array if only one element is present
		if (!is_array($xml[self::EntityName][0])) {
			if (is_array($xml[self::EntityName])) {
				$first = $xml[self::EntityName];
				$xml[self::EntityName] = array();
				$xml[self::EntityName][] = $first;
			}
		}
		
		foreach($xml[self::EntityName] as $pulserecord) {
			$query = "";
			$values = "";
			$bindParam = new BindParam();
			$qArray = array();
			$vArray = array();
			
			for ($i = 0; $i < count(self::$TABLE_ARRAY_STRUCT); $i++) {
				$key = self::$TABLE_ARRAY_STRUCT["$Table_Array_Keys[$i]"];
				$qArray[] = $key;
				$vArray[] = '?';
				
				$value = $pulserecord["$key"];
				if (is_array($value)) {
					$value = "";
				}
				$type = self::$TABLE_ARRAY_TYPES[$i];
				
				echo "value" . $value;
				$bindParam->add($type, $value);
			}
				
			$query .= implode(', ', $qArray);
			$values .= implode(', ', $vArray);
			
			$resultSql = "INSERT INTO " . self::DB_prefix . self::DB_TABLE . " (" . $query . ") VALUES (" . $values . ");";
			
			echo $resultSql;
			$stmt = $this->db->prepare($resultSql);
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
				
			call_user_func_array( array($stmt, 'bind_param'), $bindParam->get());
			$stmt->execute();
			if (!$stmt) {
				$error = true;
				echo ("Wrong SQL: Error: " . $stmt->errno . " " . $stmt->error);
			}
		
		}
		
		$stmt->close();
		
		if ($error) {
			return null;
		}
		return 1;
	}
	
	
    // Main method 
    function handlePulseRecordsRequests($requestMethod) {
		
    	
    	// /webservice/pulseexercises/  GET all pulseexercises
    	if (strcmp($requestMethod, 'GET') == 0) {
    		$method = $_GET["method"];
    		 
    		if (strcmp($method, 'delete') != 0) {
    			 
    			$result = $this->getPulseRecords();
    	
    			if ($result != null) {
    				$this->rest->sendResponse(200, $this->rest->xml_encode($result), 'application/xml');
    				return true;
    			} else {
    				$this->rest->sendResponse(200, $this->rest->xml_encode(""));
    				return false;
    			}
    	
    		} else {
    	
    			$Ids = $_GET["Ids"];
    	
    			if ($Ids != null) {
    	
    				$ret = $this->deletePulseRecords($Ids);
    				$this->rest->sendResponse(200, $ret);
    			}
    		}
    		 
    		 
    	}
    	
    	// /webservice/exercises/
    	// POST (data contains XML structure) save all exercises
    	
    	if (strcmp($requestMethod, 'POST') == 0) {
    			
    		$method = $_POST["method"];
    			
    		if (strcmp($method, 'update') != 0) {

    			$data = null;
    			$data = $_POST["data"];

    			if ($data != null) {
    				$XmlObjectData = $this->rest->xml_decode($data);
    				$ret = $this->savePulseRecords($XmlObjectData);
    					
    				if ($ret != null) {
    					$this->rest->sendResponse(200, $ret);
    					return true;
    				} else {
    					$this->rest->sendResponse(500, "Unable to save data!");
    					return false;
    				}
    	
    			}
    				
    		} else {
    	
    			$data = null;
    			$data = $_POST["data"];
    			$Ids = $_POST["Ids"];
    				
    			if ($data != null) {
    				$XmlObjectData = $this->rest->xml_decode($data);
    				$ret = $this->updatePulseRecords($XmlObjectData, $Ids);
    	
    				if ($ret != null) {
    					$this->rest->sendResponse(200, $ret);
    					return true;
    				} else {
    					$this->rest->sendResponse(500, "Unable to update data!");
    					return false;
    				}
    	
    			}
    		}
    			
    	}
    	
    	
//         if ($requestMethod=='GET') {
//         	$result = $this->getPulseRecords();
        	
// 			if ($result != null) {
// 				$this->rest->sendResponse(200, $this->rest->xml_encode($result), 'application/xml');
// 				return true;	
// 			} else {
// 				$this->rest->sendResponse(200, $this->rest->xml_encode(""));
// 				return false;
// 			}
// 		}
		
		// /webservice/exercises/  
		// POST (data contains XML structure) save all exercises
/* 		
		if ($requestMethod=='POST') {
			
			$data = null;
			$data = $_POST["data"];
			if ($data != null) {
				$XmlObjectData = $this->rest->xml_decode($data);
				$ret = $this->savePulseRecords($XmlObjectData);
				
				if ($ret != null) {
					$this->rest->sendResponse(200, $ret);
					return true;
				} else {
					$this->rest->sendResponse(500, "Unable to save data!");
					return false;
				} 
					
			}
		}

		if ($requestMethod == 'PUT') {
			
			$data = null;
			$data = $_POST["data"];
			$Ids = $_POST["Ids"];
			
			if ($data != null) {
				$XmlObjectData = $this->rest->xml_decode($data);
				$ret = $this->updatePulseRecords($XmlObjectData, $Ids);
			
				if ($ret != null) {
					$this->rest->sendResponse(200, $ret);
					return true;
				} else {
					$this->rest->sendResponse(500, "Unable to update data!");
					return false;
				}
					
			}
		}
		
		if ($requestMethod == 'DELETE') {
				
			$Ids = $_POST["Ids"];
				
			if ($Ids != null) {

				$ret = $this->deletePulseRecords($Ids);
				$this->rest->sendResponse(200, $ret);
			}
		} */
	}

}


$api = new MasterSport;
$pulseRecords = new PulseRecords;
$pulseRecords->setDb($api->db);
$pulseRecords->setRest($api->rest);
$pulseRecords->handlePulseRecordsRequests($_SERVER['REQUEST_METHOD']);
?>